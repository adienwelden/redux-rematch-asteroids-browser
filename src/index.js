import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import './index.css';
import App from './App';
import store from './store';
import * as serviceWorker from './serviceWorker';

const Root = () =>
    <Provider store={store}>
        <App />
    </Provider>

const mountApp = document.getElementById('root');

ReactDOM.render(<Root />, mountApp);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
