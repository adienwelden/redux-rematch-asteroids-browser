import React from 'react';
import Item from './Item';

const List = props => props.data ?
    <div>
        {
            props.data.map(elem =>
                <Item key={elem.neo_reference_id} data={elem} />
            )
        }
    </div>
    :
    null;

export default List;