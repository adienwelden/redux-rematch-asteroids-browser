import { getAsteroids } from '../repositories';

const initialState = {
    asteroids: null,
}

export const asteroids = {
    state: initialState,
    reducers: {
        getAsteroids(state, payload) {
            return {
                ...state,
                asteroids: [ ...payload ],
            }
        },
        clearStore() {
            return initialState;
        }
    },
    effects: dispatch => ({
        async getAsteroidsAsync(payload, rootState) {
            try {
              if(rootState.asteroids.asteroids) {
                return Promise.resolve();
              }
              const response = await getAsteroids();
              const data = response.data.near_earth_objects ? response.data.near_earth_objects : []
              return dispatch.asteroids.getAsteroids(data);
            } catch (e) {
              console.log("ERRO EM getAsteroidsAsync, getAsteroids()", e);
              throw e
            }
          },
         })
};