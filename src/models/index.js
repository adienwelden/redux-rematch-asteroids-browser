import { asteroids } from "./asteroids";

const models = {
    asteroids,
};

export default models;