import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';

import List from './components/List';

class App extends Component {

  componentDidMount() {
    this.props.getAsteroids();
  }

  render() {
    const { asteroids } = this.props;
    return (
      <div className="App">
          <List
            data={asteroids}
          />
      </div>
    );
  }
}

const mapState = state => ({
  asteroids: state.asteroids.asteroids,
});

const mapDispatch = dispatch => ({
  getAsteroids: () => dispatch.asteroids.getAsteroidsAsync(),
});

export default connect(mapState, mapDispatch)(App);
