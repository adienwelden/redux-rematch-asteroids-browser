import axios from "axios";
import { appConfig } from "./constants";

export const instance = API => {
  const config = {
    baseURL: appConfig.nasaUrl + '?api_key=' + appConfig.nasaKey,
  };
  const instance = axios.create(config);
  instance.interceptors.response.use(
      response => response,
      error => {
        if (error && error.response && error.response.data.message) {
          console.log("ERRO", error.response.data.message);
        }
        return Promise.reject(error);
  });
  return instance;
}
