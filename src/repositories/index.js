import { instance } from '../utils/api';

export const getAsteroids = () =>
    instance()
        .get()
        .then(res => res)
        .catch(e => {
            throw e;
        });
